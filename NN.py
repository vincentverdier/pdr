from time import time
from keras.callbacks import TensorBoard
import tensorflow as tf
import numpy as np
import pandas as pd
import keras

class NN(keras.Model):

    def __init__(self, use_bn=False, use_dp=False, num_classes=1):
        super(NN, self).__init__(name='nn')
        self.use_bn = use_bn
        self.use_dp = use_dp
        self.num_classes = num_classes

        self.dense1 = keras.layers.Dense(300, activation='relu')
        self.dense2 = keras.layers.Dense(300, activation='relu')
        self.dense3 = keras.layers.Dense(300, activation='relu')
        self.dense4 = keras.layers.Dense(200, activation='relu')
        self.dense5 = keras.layers.Dense(50, activation='relu')
        self.dense6 = keras.layers.Dense(num_classes, activation='sigmoid')
        if self.use_dp:
            self.dp = keras.layers.Dropout(0.7)
        if self.use_bn:
            self.bn = keras.layers.BatchNormalization(axis=-1)

    def call(self, inputs):
        x = self.dense1(inputs)
        if self.use_dp:
            x = self.dp(x)
        if self.use_bn:
            x = self.bn(x)

        x = self.dense2(x)
        if self.use_dp:
            x = self.dp(x)

        x = self.dense3(x)
        if self.use_dp:
            x = self.dp(x)

        x = self.dense4(x)
        if self.use_dp:
            x = self.dp(x)

        x = self.dense5(x)
        if self.use_dp:
            x = self.dp(x)

        return self.dense6(x)
