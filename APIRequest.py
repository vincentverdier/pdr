import numpy as np
import pandas as pd
import requests
import json
import csv
import time
import os
import sys

class APIRequest:

    def __init__(self, nbSteps):
        self.key = "RGAPI-d7529afe-05fb-4f80-9623-a0fdc959b067"
        self.leagues = ["challengerleagues", "grandmasterleagues", "masterleagues"]
        self.gamemodes = ["RANKED_SOLO_5x5", "RANKED_FLEX_SR"]
        self.features = {'currentGold','totalGold','level','xp','minionsKilled','jungleMinionsKilled'}
        self.nbSteps = nbSteps
        self.nbJoueurs = 10


    ##### Tools #####

    def checkForDuplicates(self, filename):
        file = pd.read_csv(filename, sep=",", header=None)
        file.drop_duplicates(subset=None, inplace=True)
        newFile = filename.replace(".old", "")
        file.to_csv(newFile, mode="w", index=False, header=False)

    def progress(self, count, total, suffix='complete', name=''):
        bar_len = 60
        filled_len = int(round(bar_len * count / float(total)))

        percents = round(100.0 * count / float(total), 1)
        bar = '█' * filled_len + '-' * (bar_len - filled_len)

        sys.stdout.write('[%s] %s%s %s (%s %s)\r' % (bar, percents, '%', suffix, count, name))
        sys.stdout.flush()
    #################

    ##### Requests #####


    def getSummonnerIDs(self):
        accountRequests = []
        for league in self.leagues :
            for gamemode in self.gamemodes :
                accountRequests.append('https://euw1.api.riotgames.com/lol/league/v4/' + league + '/by-queue/' + gamemode + "?api_key=" + self.key)

        summoners = csv.writer(open('data/summoners.old.csv', 'w+', newline=''))

        for request in accountRequests:
            response = requests.get(request)
            responseJSON = response.json()
            for entry in responseJSON['entries']:
                summoners.writerow([entry['summonerId']])

        self.checkForDuplicates('data/summoners.old.csv')


    def getAccountIDs(self):
        summoners = pd.read_csv('data/summoners.csv', sep=',', header=None).values
        accounts = csv.writer(open('data/accounts.old.csv', 'w+', newline=''))

        for summonerID in summoners:
            accountRequest = 'https://euw1.api.riotgames.com/lol/summoner/v4/summoners/' + str(summonerID[0]) + '?api_key=' + self.key
            response = requests.get(accountRequest)
            accountData = response.json()

            accounts.writerow([accountData['accountId']])
            time.sleep(1.2)

        self.checkForDuplicates('data/accounts.old.csv')


    def getGameIDs(self, nbreComptes):
        accounts = pd.read_csv('data/accounts.csv', sep=',', header=None).values
        games = csv.writer(open('data/games.old.csv', 'w+', newline=''))

        self.progress(0, nbreComptes, name="comptes")

        for index, accountID in enumerate(accounts):
            if (index >= nbreComptes):
                break

            gamesRequest = "https://euw1.api.riotgames.com/lol/match/v4/matchlists/by-account/" + accountID[0] + "?api_key=" + self.key
            response = requests.get(gamesRequest)
            gamesData = response.json()

            for match in gamesData['matches']:
                games.writerow([match['gameId']])

            time.sleep(1.2)

        self.checkForDuplicates('data/games.old.csv')


    def getInputsAndLabels(self, nbreGames):
        games = pd.read_csv('data/games.csv', sep=',', header=None).values
        timelines = csv.writer(open('data/timelinesTuning.csv', 'w+', newline=''))
        labels = csv.writer(open('data/labelsTuning.csv', 'w+', newline=''))

        self.progress(0, nbreGames, name="games")

        for index, gameID in enumerate(games):
            if (index > nbreGames):
                break

            self.progress(index+1, nbreGames, name="games")

            gameRequest = "https://euw1.api.riotgames.com/lol/match/v4/timelines/by-match/" + str(gameID[0]) + "?api_key=" + self.key
            winRequest = "https://euw1.api.riotgames.com/lol/match/v4/matches/" + str(gameID[0]) + "?api_key=" + self.key

            gameResponse = requests.get(gameRequest)
            gameResponseJSON = gameResponse.json()
            winResponse = requests.get(winRequest)
            winResponseJSON = winResponse.json()

            if(('frames' not in gameResponseJSON) or ('teams' not in winResponseJSON)):
                continue

            frames = gameResponseJSON['frames']
            win = winResponseJSON['teams'][0]["win"]
            winningTeam = [("1", "0")[win=="Win"]]

            for indexSample in range(len(frames)-(self.nbSteps-1)):
                for indexFrame in range(self.nbSteps):
                    frameData = [indexSample + indexFrame]
                    for indexJoueur in range (self.nbJoueurs):
                        currentFrame = frames[indexSample+indexFrame]["participantFrames"][str(indexJoueur+1)]
                        for feature in self.features:
                            frameData.append(currentFrame[str(feature)])
                    timelines.writerow(frameData)
                timelines.writerow([])
                labels.writerow(winningTeam)

            time.sleep(2.5)

    def concatenateFrames(self):
        feats = ['xp','currentGold','totalGold','jglMinions','lvl','minions']
        strAttr='win,'
        for i in range(5):
            strAttr = strAttr+ 'frameNumber' + str(i)+','
            for j in range(10):
                for k in range(6):
                    print(feats[k]+"J"+str(j)+"F"+str(i)+',')
                    strAttr = strAttr + feats[k] + "J" + str(j) + "F" + str(i)+','
        strAttr=strAttr[:-1]
        #print(strAttr)


        data = pd.read_csv('data/timelines.csv',sep=',',header=None).values
        print(data.shape)
        data = np.reshape(data, (int(data.shape[0]/5), int(data.shape[1]*5)))
        win = pd.read_csv('data/labels.csv',header=None).values
        for index,gg in enumerate(win):
            np.insert(data[index],1,gg)

        np.savetxt('data/timelinesweka.csv',data,delimiter=',',fmt='%.0f',header=strAttr)


    def getRepartition(self,fileJSON):
        tab=np.zeros(10)
        for i in range(10):
            if(fileJSON['participants'][i]['timeline']['lane'] == 'TOP'):
                tab[i]=0 #Role TOP
            if(fileJSON['participants'][i]['timeline']['lane'] == 'JUNGLE'):
                tab[i]=1 #Role JGL
            if(fileJSON['participants'][i]['timeline']['lane'] == 'MIDDLE'):
                tab[i]=2 #Role MID
            if(fileJSON['participants'][i]['timeline']['lane'] == 'BOTTOM' and fileJSON['participants'][i]['timeline']['role'] == 'DUO_CARRY'):
                tab[i]=3 #Role ADC
            if(fileJSON['participants'][i]['timeline']['lane'] == 'BOTTOM' and fileJSON['participants'][i]['timeline']['role'] == 'DUO_SUPPORT'):
                tab[i]=4 #Role support
        return tab

    def ordonner(self, frameData, repartitionRoles):
        newData = []
        for i in range(5):
            get_indexes = lambda x, xs: [i for (y, i) in zip(xs, range(len(xs))) if x == y]
            players = get_indexes(i, repartitionRoles)
            newData = np.append(newData, frameData[6*players[0] : 6*players[0] + 6])
            newData = np.append(newData, frameData[6*players[1] : 6*players[1] + 6])
        return newData.astype(int)

    def getRepartitionValide(self,tab):
        roles=np.zeros(5)
        for i in range(10):
            roles[int(tab[i])]+=1
        return (roles[0]==2 and roles[1]==2 and roles[2]==2 and roles[3]==2 and roles[4]==2)

    def getGamesOver20ClassifiedByTeam(self, nbreGames):
        feats = ['xp','currentGold','totalGold','jungleMinionsKilled','level','minionsKilled']
        games = pd.read_csv('data/dataArchives 5-8-2019/gamesValides4.csv', sep=',', header=None).values
        timelines = csv.writer(open('data/dataWeka/timelines.csv', 'w+', newline=''))
        labels = csv.writer(open('data/dataWeka/labels.csv', 'w+', newline=''))
        gamesValides = csv.writer(open('data/dataWeka/gamesValides.csv','w+',newline=''))

        compteGame=0

        for index, gameID in enumerate(games):
            if (index > nbreGames):
                break
            try:
                gameRequest = "https://euw1.api.riotgames.com/lol/match/v4/timelines/by-match/" + str(gameID[0]) + "?api_key=" + self.key
                winRequest = "https://euw1.api.riotgames.com/lol/match/v4/matches/" + str(gameID[0]) + "?api_key=" + self.key

                gameResponse = requests.get(gameRequest)
                gameResponseJSON = gameResponse.json()
                frames = gameResponseJSON['frames']
                winResponse = requests.get(winRequest)
                winResponseJSON = winResponse.json()
                repartitionRoles=self.getRepartition(fileJSON=winResponseJSON)
                repartitionValide=self.getRepartitionValide(tab=repartitionRoles)

                win = winResponseJSON['teams'][0]["win"]
                winningTeam = ("1", "0")[win=="Win"]

                if len(frames)>20 and repartitionValide:
                    compteGame=compteGame+1
                    print('GamesValides:', compteGame)
                    gamesValides.writerow([gameID[0]])
                    for indexFrame in range(self.nbSteps):
                        frameData=[]
                        for indexJoueur in range(self.nbJoueurs):
                            currentFrame = frames[13+indexFrame]["participantFrames"][str(indexJoueur+1)]
                            for k in range(6):
                                frameData.append(currentFrame[str(feats[k])])
                        dataOrdonnee = self.ordonner(frameData,repartitionRoles)
                        timelines.writerow(dataOrdonnee)
                    timelines.writerow([])
                    labels.writerow([winningTeam])
                time.sleep(2.5)
            except Exception as e :
                print(str(e))
                pass

    def concatenateClassifiedFrames(self):
        feats = ['xp','currentGold','totalGold','jungleMinionsKilled','level','minionsKilled']
        role = ['top','jgl','mid','adc','supp']
        strAttr='win,'
        for i in range(13,18):
            for j in range(5):
                for k in range(6):
                    strAttr = strAttr + feats[k] + role[j%5] + "Eq0" + "F" + str(i)+','
                for k in range(6):
                    strAttr = strAttr + feats[k] + role[j%5] + "Eq1" + "F" + str(i)+','
        strAttr=strAttr[:-1]


        data = pd.read_csv('data/dataWeka/wholeTimelinesTreated.csv',sep=',',header=None).values
        print(data.shape)
        data = np.reshape(data, (int(data.shape[0]/5), int(data.shape[1]*5)))
        win = pd.read_csv('data/dataWeka/wholeLabelsTreated.csv',header=None).values
        data=np.concatenate((win,data),axis=1)


        np.savetxt('data/timelinesweka.csv',data,delimiter=',',fmt='%.0f',header=strAttr)

    def getTotalGames(self,nbreGames):
        games=pd.read_csv('data/dataArchives 5-8-2019/gamesValides4.csv',sep=',',header=None).values
        for index,gameID in enumerate(games):
            if (index>nbreGames):
                break
            try:
                timelines=open('data/dataTotale/wholeTimelines/' + str(gameID[0])+'.json','w+',newline='',encoding='utf-8')
                labels=open('data/dataTotale/wholeGames/' + str(gameID[0])+'.json','w+',newline='',encoding='utf-8')
                gameRequest = "https://euw1.api.riotgames.com/lol/match/v4/timelines/by-match/" + str(gameID[0]) + "?api_key=" + self.key
                winRequest = "https://euw1.api.riotgames.com/lol/match/v4/matches/" + str(gameID[0]) + "?api_key=" + self.key

                gameResponse=requests.get(gameRequest)
                gameResponseJSON=gameResponse.json()

                timelines.write(json.dumps(gameResponseJSON,indent=4))
                winResponse=requests.get(winRequest)
                winResponseJSON=winResponse.json()

                labels.write(json.dumps(winResponseJSON,indent=4))
                timelines.close()
                labels.close()
                print("Game" + str(index) + "Ajoutée ! ")
                time.sleep(2.5)
            except Exception as e:
                print(str(e))
                time.sleep(2.5)
                pass
