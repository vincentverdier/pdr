from time import time
from keras.callbacks import TensorBoard
from keras.constraints import max_norm
import tensorflow as tf
import numpy as np
import pandas as pd
import keras

class RNN:

    def __init__(self, mode, nbFeatures, nbSteps, LSTMLayers, denseLayers, epochs, batchSize, learningRate, momentum, activation, weightConstraint, dropout):
        self.nbFeatures = nbFeatures
        self.nbSteps = nbSteps
        self.LSTMLayers = LSTMLayers
        self.denseLayers = denseLayers
        self.epochs = epochs
        self.batchSize = batchSize
        self.learningRate = learningRate
        self.momentum = momentum
        self.activation = activation
        self.weightConstraint = weightConstraint
        self.dropout = dropout
        self.decay = self.learningRate/self.epochs

        if(mode=='train'):
            self.load_data_from_csv()
        if(mode=='hypertuning'):
            self.load_data_from_csv_tuning()

        self.create_model()


    def create_model(self):
        self.model = keras.Sequential()

        self.model.add(keras.layers.BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True))

        self.model.add(keras.layers.LSTM(self.LSTMLayers[0], return_sequences=True, dropout=self.dropout, kernel_constraint=max_norm(self.weightConstraint), input_shape=(self.nbSteps, self.nbFeatures)))

        for index in range(1, len(self.LSTMLayers)-1):
            self.model.add(keras.layers.LSTM(self.LSTMLayers[index], return_sequences=True, dropout=self.dropout, kernel_constraint=max_norm(self.weightConstraint)))

        self.model.add(keras.layers.LSTM(self.LSTMLayers[len(self.LSTMLayers)-1], dropout=self.dropout, kernel_constraint=max_norm(self.weightConstraint)))


        for neurons in self.denseLayers:
            self.model.add(keras.layers.Dense(neurons, kernel_constraint=max_norm(self.weightConstraint), activation=self.activation))
            self.model.add(keras.layers.Dropout(self.dropout))

        self.model.add(keras.layers.Dense(1, activation='sigmoid'))

        optimizer = keras.optimizers.RMSprop(lr=self.learningRate, rho=self.momentum, decay=self.decay)
        self.model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])


    def load_data_from_csv(self):
        self.inputs = pd.read_csv('data/timelines.csv', sep=',', header=None).values
        self.labels = pd.read_csv('data/labels.csv', sep=',', header=None).values

        self.nb_samples = int(self.inputs.shape[0] / self.nbSteps)

        self.inputs = self.inputs.reshape(self.nb_samples, self.nbSteps, self.nbFeatures)
        self.labels = self.labels.reshape(self.inputs.shape[0])


    def load_data_from_csv_tuning(self):
        self.inputs = pd.read_csv('data/timelinesTuning.csv', sep=',', header=None).values
        self.labels = pd.read_csv('data/labelsTuning.csv', sep=',', header=None).values

        self.nb_samples = int(self.inputs.shape[0] / self.nbSteps)

        self.inputs = self.inputs.reshape(self.nb_samples, self.nbSteps, self.nbFeatures)
        self.labels = self.labels.reshape(self.inputs.shape[0])


    def train(self):
        self.tensorboard = TensorBoard(log_dir="logs/lr-"+str(self.learningRate)+";lstm-"+str(self.LSTMLayers)+";dense-"+str(self.denseLayers)+";epochs-"+str(self.epochs)+";batch-"+str(self.batchSize)+";moment-"+str(self.momentum)+";act-"+str(self.activation)+";drop-"+str(self.dropout)+";weight-"+str(self.weightConstraint))
        self.model.fit(self.inputs, self.labels, epochs=self.epochs, batch_size=self.batchSize, shuffle=True, verbose=1, steps_per_epoch=None, validation_split=0.2, callbacks=[self.tensorboard])
