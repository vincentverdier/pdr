from time import time
from keras.callbacks import TensorBoard
from keras import regularizers
import tensorflow as tf
import numpy as np
import pandas as pd
import keras

class autoencoder(keras.Model):

    def __init__(self, input_size):
        super(autoencoder, self).__init__(name='autoencoder')
        self.bn = keras.layers.BatchNormalization(axis=-1)

        self.encoder1 = keras.layers.Dense(input_size, activation='relu')
        self.encoder2 = keras.layers.Dense(200, activation='relu')
        self.encoder3 = keras.layers.Dense(100, activation='relu')
        self.encoder4 = keras.layers.Dense(80, activation='relu')
        self.decoder1 = keras.layers.Dense(100, activation='relu')
        self.decoder2 = keras.layers.Dense(200, activation='relu')
        self.decoder3 = keras.layers.Dense(input_size, activation='sigmoid')

    def call(self, inputs):
        x = self.encoder1(inputs)
        x = self.bn(x)
        x = self.encoder2(x)
        x = self.encoder3(x)
        x = self.encoder4(x)
        x = self.decoder1(x)
        x = self.decoder2(x)
        return self.decoder3(x)
