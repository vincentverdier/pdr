import numpy as np
import pandas as pd
import requests
import json
import csv
import time
import os
import sys
import APIRequest as API

class DataTreatment:

    def treatDataOldWay(self):
        HAPI = API.APIRequest(nbSteps=5)
        feats = ['xp','currentGold','totalGold','jungleMinionsKilled','level','minionsKilled']
        resultingTimeline = csv.writer(open('data/dataWeka/wholeTimelinesTreated.csv', 'w+', newline=''))
        resultingLabels=csv.writer(open('data/dataWeka/wholeLabelsTreated.csv', 'w+', newline=''))
        for f in os.listdir("data/dataWeka/wholeGames"):
            fileGames=open("data/dataWeka/wholeGames/"+f)
            dataGames=json.load(fileGames)
            
            fileTimelines=open("data/dataWeka/wholeTimelines/"+f)
            dataTimelines=json.load(fileTimelines)
            #print(dataTimelines['frames'])
            if 'frames' not in dataTimelines:
                fileTimelines.close()
                fileGames.close()
                continue
            frames=dataTimelines['frames']
            repartitionRoles = HAPI.getRepartition(fileJSON=dataGames)
            win = dataGames['teams'][0]["win"]
            winningTeam = ("1","0")[win=="Win"]

            for indexFrame in range(5):
                frameData=[]
                for indexJoueur in range(10):
                    currentFrame = frames[13+indexFrame]["participantFrames"][str(indexJoueur+1)]
                    for k in range(6):
                        frameData.append(currentFrame[str(feats[k])])
                dataOrdonnee=HAPI.ordonner(frameData,repartitionRoles)
                resultingTimeline.writerow(dataOrdonnee)
            resultingTimeline.writerow([])
            resultingLabels.writerow([winningTeam])
            fileTimelines.close()
            fileGames.close()
