
import NN as NN
import os
import keras
from keras.callbacks import TensorBoard
import pandas as pd
import numpy as np
from time import time

model = NN.NN(use_bn=False, use_dp=False, num_classes=1)

#sgd = keras.optimizers.SGD(lr=0.01, momentum=0.0, decay=0.0, nesterov=False)
#rms = keras.optimizers.RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)
#adagrad = keras.optimizers.Adagrad(lr=0.01, epsilon=None, decay=0.0)
#adadelta = keras.optimizers.Adadelta(lr=1.0, rho=0.95, epsilon=None, decay=0.0)
adam = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
#adamax = keras.optimizers.Adamax(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0)
#nadam = keras.optimizers.Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=None, schedule_decay=0.004)


model.compile(loss='binary_crossentropy', optimizer=adam , metrics=['accuracy'])
tensorboard = TensorBoard(log_dir="logs/{}".format(time()))

script_dir = os.getcwd()
file1 = 'data/dataWeka/wholeTimelinesTreated.csv'
file2 = 'data/dataWeka/wholeLabelsTreated.csv'

inputs = pd.read_csv(os.path.normcase(os.path.join(script_dir, file1)), sep=',', header=None, engine='c').values
inputs = np.reshape(inputs, (int(inputs.shape[0]/5), int(inputs.shape[1]*5)))
labels = pd.read_csv(os.path.normcase(os.path.join(script_dir, file2)), sep=',', header=None, engine='c').values

model.fit(inputs, labels, epochs=100, batch_size=10, shuffle=True, verbose=2, steps_per_epoch=None, validation_split=0.2 ,callbacks=[tensorboard])
model.summary()